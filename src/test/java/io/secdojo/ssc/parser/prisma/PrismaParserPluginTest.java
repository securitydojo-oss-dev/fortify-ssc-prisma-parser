package io.secdojo.ssc.parser.prisma;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

import com.fortify.plugin.api.ScanBuilder;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.ScanEntry;
import com.fortify.plugin.api.StaticVulnerabilityBuilder;
import com.fortify.plugin.api.VulnerabilityHandler;

class PrismaParserPluginTest {
	private static final String[] SAMPLE_FILES = {
			"prisma.json",
	};
	
	private final ScanData getScanData(final String fileName) {
		return new ScanData() {
		
			@Override
			public String getSessionId() {
				return UUID.randomUUID().toString();
			}
			
			@Override
			public List<ScanEntry> getScanEntries() {
				return null;
			}
			
			@Override
			public InputStream getInputStream(Predicate<String> matcher) throws IOException {
				return ClassLoader.getSystemResourceAsStream(fileName);
			}
			
			@Override
			public InputStream getInputStream(ScanEntry scanEntry) throws IOException {
				return ClassLoader.getSystemResourceAsStream(fileName);
			}
		};
	}
	
	private final ScanBuilder scanBuilder = (ScanBuilder) Proxy.newProxyInstance(
			PrismaParserPluginTest.class.getClassLoader(),
			  new Class[] { ScanBuilder.class }, new InvocationHandler() {
				
				@Override
				public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
					System.err.println(method.getName()+": "+(args==null?null:Arrays.asList(args)));
					return null;
				}
			});
	
	private final VulnerabilityHandler vulnerabilityHandler = new VulnerabilityHandler() {
		
		@Override
		public StaticVulnerabilityBuilder startStaticVulnerability(String instanceId) {
			System.err.println("startStaticVulnerability: "+instanceId);
			return (StaticVulnerabilityBuilder) Proxy.newProxyInstance(
					PrismaParserPluginTest.class.getClassLoader(),
					  new Class[] { StaticVulnerabilityBuilder.class }, new InvocationHandler() {
						
						@Override
						public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
							System.err.println(method.getName()+": "+(args==null?null:Arrays.asList(args)));
							return null;
						}
					}); 
		}
	};
	
	@Test
	void testParseScan() throws Exception {
		for ( String file : SAMPLE_FILES ) {
			System.err.println("\n\n---- "+file+" - parseScan");
			new PrismaParserPlugin().parseScan(getScanData(file), scanBuilder);
			// TODO Check actual output
		}
	}
	
	@Test
	void testParseVulnerabilities() throws Exception {
		for ( String file : SAMPLE_FILES ) {
			System.err.println("\n\n---- "+file+" - parseVulnerabilities");
			new PrismaParserPlugin().parseVulnerabilities(getScanData(file), vulnerabilityHandler);
			// TODO Check actual output
		}
	}

}
