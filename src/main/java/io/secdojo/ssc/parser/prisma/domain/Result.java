package io.secdojo.ssc.parser.prisma.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class Result {

    @JsonProperty("id")
    private String id;
    @JsonProperty("distro")
    private String distro;
    // public List<Compliance> compliances;
    // public ComplianceDistribution complianceDistribution;
    @JsonProperty("vulnerabilities")
    private Vulnerability[] vulnerabilities;
    // public VulnerabilityDistribution vulnerabilityDistribution;

}

