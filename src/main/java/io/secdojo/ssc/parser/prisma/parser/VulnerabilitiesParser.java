package io.secdojo.ssc.parser.prisma.parser;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.secdojo.ssc.parser.prisma.domain.Result;
import io.secdojo.ssc.parser.prisma.CustomVulnAttribute;
import org.apache.commons.codec.digest.DigestUtils;

import com.fortify.plugin.api.BasicVulnerabilityBuilder.Priority;
import com.fortify.plugin.api.FortifyAnalyser;
import com.fortify.plugin.api.FortifyKingdom;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.ScanParsingException;
import com.fortify.plugin.api.StaticVulnerabilityBuilder;
import com.fortify.plugin.api.VulnerabilityHandler;
import io.secdojo.ssc.parser.prisma.domain.Vulnerability;
import com.fortify.util.ssc.parser.EngineTypeHelper;
import com.fortify.util.ssc.parser.json.ScanDataStreamingJsonParser;

public class VulnerabilitiesParser {
    private static final String ENGINE_TYPE = EngineTypeHelper.getEngineType();
    private final ScanData scanData;
    private final VulnerabilityHandler vulnerabilityHandler;

    private static final Map<String, Priority> MAP_SEVERITY_TO_PRIORITY = Stream.of(
            new AbstractMap.SimpleImmutableEntry<>("low", Priority.Low),
            new AbstractMap.SimpleImmutableEntry<>("moderate", Priority.Medium),
            new AbstractMap.SimpleImmutableEntry<>("high",Priority.High),
            new AbstractMap.SimpleImmutableEntry<>("critical",Priority.Critical))
            .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));

    public VulnerabilitiesParser(final ScanData scanData, final VulnerabilityHandler vulnerabilityHandler) {
        this.scanData = scanData;
        this.vulnerabilityHandler = vulnerabilityHandler;
    }

    /**
     * Main method to commence parsing the input provided by the configured {@link ScanData}.
     *
     * @throws ScanParsingException
     * @throws IOException
     */
    public final void parse() throws ScanParsingException, IOException {
        new ScanDataStreamingJsonParser()
                .handler("/results/*", Result.class, this::handleResults)
                .parse(scanData);
    }

    private void handleResults(Result result) {
        for (Vulnerability vuln : result.getVulnerabilities()) {
            buildVulnerability(vuln);
        }
    }

    private void buildVulnerability(Vulnerability vulnerability) {
        StaticVulnerabilityBuilder vb = vulnerabilityHandler.startStaticVulnerability(getInstanceId(vulnerability));
        vb.setEngineType(ENGINE_TYPE);
        vb.setKingdom(FortifyKingdom.ENVIRONMENT.getKingdomName());
        vb.setAnalyzer(FortifyAnalyser.CONFIGURATION.getAnalyserName());
        vb.setCategory("VULNERABLE CONTAINER/INFRASTRUCTURE");

        // Set mandatory values to JavaDoc-recommended values
        vb.setAccuracy(5.0f);
        vb.setConfidence(2.5f);
        vb.setLikelihood(2.5f);

        vb.setFileName(vulnerability.getPackageName());
        vb.setVulnerabilityAbstract(vulnerability.getDescription());
        vb.setPriority(getPriority(vulnerability));

        vb.setStringCustomAttributeValue(CustomVulnAttribute.id, vulnerability.getId());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.status, vulnerability.getStatus());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.cvss, vulnerability.getCvss());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.vector, vulnerability.getVector());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.packageName, vulnerability.getPackageName());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.packageVersion, vulnerability.getPackageVersion());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.link, getURL(vulnerability));
        vb.setStringCustomAttributeValue(CustomVulnAttribute.publishedDays, vulnerability.getPublishedDays());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.riskFactors, getRiskFactors(vulnerability));

        vb.completeVulnerability();

    }

    private String getInstanceId(Vulnerability vulnerability) {
        return DigestUtils.sha256Hex(vulnerability.getId() + vulnerability.getPackageName() + vulnerability.getPackageVersion());
    }

    private Priority getPriority(Vulnerability vulnerability) {
        return MAP_SEVERITY_TO_PRIORITY.getOrDefault(vulnerability.getSeverity(), Priority.Medium);
    }

    private String getURL(Vulnerability vulnerability){
        return "<a href=\"" + vulnerability.getLink() + "\">" + vulnerability.getLink() + "</a>";
    }

    private String getRiskFactors(Vulnerability vulnerability){
        Pattern p = Pattern.compile("\"([^\"]*)\"");
        Matcher m = p.matcher(vulnerability.getRiskFactors().toString());
        StringBuilder sb = new StringBuilder();
        while (m.find()) {
            sb.append(m.group(1)).append("</br>");
        }
        return sb.toString();
    }
}
