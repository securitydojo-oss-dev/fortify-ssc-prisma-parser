package io.secdojo.ssc.parser.prisma;
public enum CustomVulnAttribute implements com.fortify.plugin.spi.VulnerabilityAttribute {
    id(AttrType.STRING),
    status(AttrType.STRING),
    cvss(AttrType.STRING),
    vector(AttrType.STRING),
    packageName(AttrType.STRING),
    packageVersion(AttrType.STRING),
    link(AttrType.LONG_STRING),
    publishedDays(AttrType.STRING),
    riskFactors(AttrType.LONG_STRING)

    ;

    private final AttrType attributeType;

    CustomVulnAttribute(final AttrType attributeType) {
        this.attributeType = attributeType;
    }

    @Override
    public String attributeName() {
        return name();
    }

    @Override
    public AttrType attributeType() {
        return attributeType;
    }
}
